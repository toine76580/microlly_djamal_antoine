                #################  Microblog Microlly.net ##################




Microlly est un blog où l'on peut partager de l'information ou de l'actualité.

## Comment cela fonctionne

Il suffit de s'inscrire pour s'enregistrer.

## Visualisation des posts et son contenu

Il suffit de cliquer sur le logo Microlly.net et pour avoir le détail du contenu, il faut cliquer sur le titre du post.

## Ajout d'image

Lors de la création du post, vous pouvez joindre une image de type PNG, JPEG ou JPG.