from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from . import views

app_name = 'posts'

urlpatterns = [
    path('', views.post_list, name="list"),
    path('create/', views.post_create, name="create"),
    url(r'^(?P<slug>[\w-]+)/$', views.post_detail, name="detail"),
    path('posts/delete_post/', views.delete_post, name="delete_post"),


]
